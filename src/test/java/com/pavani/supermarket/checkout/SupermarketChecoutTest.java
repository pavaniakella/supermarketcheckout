/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

import com.pavani.supermarket.checkout.Basket.Transaction;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pavani
 */
public class SupermarketChecoutTest {

    private Basket basket;
    private static final Item APPLE = new Item("SKUAPPLE1", "GALA APPLE", new BigDecimal(0.70));
    private static final Item ORANGE = new Item("SKUORANGE1", "ORANGE", new BigDecimal(0.50));
    private static final Item PEAR = new Item("SKUPEAR1", "PEAR", new BigDecimal(0.60));

    @Before
    public void setup() {
        this.basket = new Basket();

    }

    /**
     * To test single item with 3 for price of 2
     */
    @Test
    public void calculate3pay2() {
        List<Rule> pricingRules = new ArrayList<>();
        Rule buy3Pay2 = new SingleItemRule("buy3for2", "buy3for2", PEAR, 3, new BigDecimal(PEAR.getPrice().doubleValue() * 2), 0);
        pricingRules.add(buy3Pay2);
        Transaction transaction = basket.startTransaction(pricingRules)
                .scan(PEAR)
                .scan(PEAR)
                .scan(ORANGE)
                .scan(PEAR);

        assertEquals(new BigDecimal(1.70).setScale(2, RoundingMode.HALF_UP), basket.calculateTotalPrice(transaction).setScale(2, RoundingMode.HALF_UP));

    }

    /**
     * To test single item with discount percentage
     */
    @Test
    public void calculate3for25Percentage() {
        List<Rule> pricingRules = new ArrayList<>();
        Rule buy3For25 = new SingleItemRule("buy3For25%", "buy3For25%off", PEAR, 3, null, 25);
        pricingRules.add(buy3For25);
        Transaction transaction = basket.startTransaction(pricingRules)
                .scan(PEAR)
                .scan(PEAR)
                .scan(ORANGE)
                .scan(PEAR);

        assertEquals(new BigDecimal(1.85).setScale(2, RoundingMode.HALF_UP), basket.calculateTotalPrice(transaction).setScale(2, RoundingMode.HALF_UP));

    }

    /**
     * To test single item with 3 for price of 2 in 2 sets
     */
    @Test
    public void calculatemultiple3for2() {
        List<Rule> pricingRules = new ArrayList<>();
        Rule buy3Pay2 = new SingleItemRule("buy3for2", "buy3for2", PEAR, 3, new BigDecimal(PEAR.getPrice().doubleValue() * 2), 0);
        pricingRules.add(buy3Pay2);
        Transaction transaction = basket.startTransaction(pricingRules)
                .scan(PEAR)
                .scan(PEAR)
                .scan(ORANGE)
                .scan(PEAR)
                .scan(PEAR)
                .scan(PEAR)
                .scan(PEAR);

        assertEquals(new BigDecimal(2.9).setScale(2, RoundingMode.HALF_UP), basket.calculateTotalPrice(transaction).setScale(2, RoundingMode.HALF_UP));

    }

    /**
     * To test single item with 2 for special price
     */
    @Test
    public void buy2forSpecialPrice() {
        List<Rule> pricingRules = new ArrayList<>();
        Rule buy3Pay2 = new SingleItemRule("buy3for2", "buy3for2", PEAR, 3, new BigDecimal(PEAR.getPrice().doubleValue() * 2), 0);
        pricingRules.add(buy3Pay2);
        Rule buy2PayX = new SingleItemRule("buy2PayX", "buy2PayX", APPLE, 2, new BigDecimal(1.2), 0);
        pricingRules.add(buy2PayX);
        Transaction transaction = basket.startTransaction(pricingRules)
                .scan(PEAR)
                .scan(PEAR)
                .scan(ORANGE)
                .scan(PEAR)
                .scan(PEAR)
                .scan(PEAR).scan(APPLE).scan(APPLE).scan(APPLE)
                .scan(PEAR);

        assertEquals(new BigDecimal(4.8).setScale(2, RoundingMode.HALF_UP), basket.calculateTotalPrice(transaction).setScale(2, RoundingMode.HALF_UP));

    }

    /**
     * To test buy 3 (in a set of items) and the cheapest is free
     */
    @Test
    public void cheapest1Free() {
        List<Rule> pricingRules = new ArrayList<>();
        Rule buy3Pay2 = new SingleItemRule("buy3for2", "buy3for2", PEAR, 3, new BigDecimal(PEAR.getPrice().doubleValue() * 2), 0);
        pricingRules.add(buy3Pay2);
        Rule buy2PayX = new SingleItemRule("buy2PayX", "buy2PayX", APPLE, 2, new BigDecimal(1.2), 0);
        pricingRules.add(buy2PayX);
        Rule cheapest1Free = new MultiItemRule("cheapest1Free", "cheapest1Free", Arrays.asList(APPLE, PEAR, ORANGE), 3, 0, 100);
        pricingRules.add(cheapest1Free);
        Transaction transaction = basket.startTransaction(pricingRules)
                .scan(PEAR)
                .scan(PEAR)
                .scan(PEAR)
                .scan(PEAR)
                .scan(PEAR)
                .scan(PEAR)
                .scan(ORANGE)
                .scan(PEAR)
                .scan(APPLE)
                .scan(APPLE)
                .scan(APPLE);

        assertEquals(new BigDecimal(4.90).setScale(2, RoundingMode.HALF_UP), basket.calculateTotalPrice(transaction).setScale(2, RoundingMode.HALF_UP));

    }

    /**
     * to test for each N (equals) items X, you get K items Y for free
     */
    @Test
    public void getYFreeForX() {
        List<Rule> pricingRules = new ArrayList<>();
        Rule yFreeForX = new MultiItemFreeRule("yFreeForX", "yFreeForX", 2, ORANGE, APPLE, 1);
        pricingRules.add(yFreeForX);
        Transaction transaction = basket.startTransaction(pricingRules)
                .scan(ORANGE)
                .scan(ORANGE)
                .scan(APPLE);

        assertEquals(new BigDecimal(1).setScale(2, RoundingMode.HALF_UP), basket.calculateTotalPrice(transaction).setScale(2, RoundingMode.HALF_UP));

    }

}
