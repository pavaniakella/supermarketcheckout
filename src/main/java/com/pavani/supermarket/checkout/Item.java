/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

import java.math.BigDecimal;

/**
 *
 * @author pavani
 */
public class Item implements Payable {

    private final String sku;
    private final String label;
    private BigDecimal price;

    public Item(String sku, String label, BigDecimal price) {
        this.sku = sku;
        this.label = label;
        this.price = price;

    }

    public String getSku() {
        return sku;
    }

    public String getLabel() {
        return label;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
