/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pavani
 */
public class MultiItemRule extends Rule {

    private final List<Item> items;
    private int discountPercentage;
    private int cheapestItemDiscountPercentage;

    public MultiItemRule(String ruleName, String ruleCondition, List<Item> items, int quantity, int discountPercentage, int cheapestItemDiscountPercentage) {
        super(ruleName, ruleCondition, quantity);
        checkNotNull(items, "Items cannot be null when creating a multiprice rule");
        checkArgument(((discountPercentage == 0 && cheapestItemDiscountPercentage != 0)||(discountPercentage != 0 && cheapestItemDiscountPercentage == 0)), "total discount percentage or cheapest item  discount percentage is required to create a rule");
 
        this.items = items;

        this.discountPercentage = discountPercentage;
        this.cheapestItemDiscountPercentage = cheapestItemDiscountPercentage;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public int getCheapestItemDiscountPercentage() {
        return cheapestItemDiscountPercentage;
    }

    public void setCheapestItemDiscountPercentage(int cheapestItemDiscountPercentage) {
        this.cheapestItemDiscountPercentage = cheapestItemDiscountPercentage;
    }

    @Override
    public Rule apply(List<ProcessedItem> items) {

        List<ProcessedItem> match = new ArrayList<>();
        setDiscountAmountBczOfRule(BigDecimal.ZERO);
        BigDecimal orginalPrice = BigDecimal.ZERO;

        for (ProcessedItem processedItem : items) {
            if (items.contains(processedItem) && !processedItem.isOfferApplied()) {
                match.add(processedItem);
                orginalPrice = orginalPrice.add(processedItem.getPrice());
                if (match.size() == quantity) {
                    setHasOfferOnItems(match);

                    if (discountPercentage != 0) {
                        setDiscountAmountBczOfRule(getDiscountAmountBczOfRule().add(orginalPrice.subtract(orginalPrice.multiply(new BigDecimal(discountPercentage * 100)))));
                    } else {
                        ProcessedItem cheapest = findCheapest(match);
                        setDiscountAmountBczOfRule(getDiscountAmountBczOfRule().add(cheapest.getPrice().multiply(new BigDecimal(cheapestItemDiscountPercentage).divide(new BigDecimal(100)))));
                    }
                    match.clear();
                }
            }
        }
        if (getDiscountAmountBczOfRule() != BigDecimal.ZERO) {
            return this;
        } else {
            return null;
        }
    }

    protected ProcessedItem findCheapest(List<ProcessedItem> items) {
        ProcessedItem cheapestItem = null;
        for (ProcessedItem item : items) {
            if (cheapestItem == null || cheapestItem.getPrice().compareTo(item.getPrice()) > 0) {
                cheapestItem = item;
            }
        }
        return cheapestItem;
    }

}
