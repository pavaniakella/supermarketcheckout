/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

/**
 *
 * @author pavani
 */
public class ProcessedItem extends Item {

    private boolean offerApplied;

    public ProcessedItem(Item item, boolean offerApplied) {
        super(item.getSku(), item.getLabel(), item.getPrice());
        this.offerApplied = offerApplied;
    }

    public boolean isOfferApplied() {
        return offerApplied;
    }

    public void setOfferApplied(boolean offerApplied) {
        this.offerApplied = offerApplied;
    }

}
