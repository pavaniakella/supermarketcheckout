/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pavani
 */
public class Basket {

    public class Transaction {

        private final List<Rule> pricingRules;
        private final HashMap<ProcessedItem, BigDecimal> shoppingBasket = new HashMap<>();
        private final LinkedHashMap<Payable, BigDecimal> shoppingReceipt = new LinkedHashMap<>();

        Transaction(List<Rule> pricingRules) {
            super();
            this.pricingRules = pricingRules;
        }

        public Transaction scan(Item item) {
            shoppingBasket.put(new ProcessedItem(item, false), item.getPrice());
            return this;
        }
    }

    public Transaction startTransaction(final List<Rule> pricingRules) {
        return new Transaction(pricingRules);
    }

    public BigDecimal calculateTotalPrice(final Transaction transaction) {

        final ArrayList<ProcessedItem> shoppingCartItems = new ArrayList<>(transaction.shoppingBasket.keySet());
        HashMap<Rule, BigDecimal> discountBasket = new HashMap<>();
        BigDecimal totalPrice = BigDecimal.ZERO;

        for (Rule rule : transaction.pricingRules) {
            Rule appliedRule = rule.apply(shoppingCartItems);
            if (appliedRule != null) {
                discountBasket.put(appliedRule, appliedRule.getDiscountAmountBczOfRule().negate());
            }
        }
        transaction.shoppingReceipt.clear();
        transaction.shoppingReceipt.putAll(transaction.shoppingBasket);
        transaction.shoppingReceipt.putAll(discountBasket);
        Iterator it = transaction.shoppingReceipt.entrySet().iterator();
        System.out.println("====================================================");
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();

            totalPrice = totalPrice.add((BigDecimal) pair.getValue());
            if (pair.getKey() instanceof ProcessedItem) {
                System.out.println(((ProcessedItem) pair.getKey()).getLabel() + " ----------------------------- " +((BigDecimal) pair.getValue()).setScale(2, RoundingMode.HALF_UP));
            } else if (pair.getKey() instanceof Rule) {
                System.out.println(((Rule) pair.getKey()).getRuleCondition() + " ----------------------------- " + ((BigDecimal) pair.getValue()).setScale(2, RoundingMode.HALF_UP));
            }

        }
        System.out.println("====================================================");
        System.out.println("Total =====" + totalPrice.setScale(2, RoundingMode.HALF_UP));
        System.out.println("====================================================");

        return totalPrice;
    }

}
