/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

import static com.google.common.base.Preconditions.checkNotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author pavani
 */
public abstract class Rule implements Payable {

    private static final AtomicInteger uniqueId = new AtomicInteger();
    private final int ruleId;
    private String ruleName;
    private String ruleCondition;
    int quantity;
    private BigDecimal discountAmountBczOfRule;

    /**
     *
     * @param ruleName
     * @param ruleCondition
     * @param quantity
     */
    public Rule(String ruleName, String ruleCondition, int quantity) {

        checkNotNull(ruleName, "Rule name cannot be null");
        checkNotNull(ruleCondition, "Rule condition cannot be null");

        this.ruleId = uniqueId.getAndIncrement();
        this.ruleName = ruleName;
        this.ruleCondition = ruleCondition;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getRuleId() {
        return ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleCondition() {
        return ruleCondition;
    }

    public void setRuleCondition(String ruleCondition) {
        this.ruleCondition = ruleCondition;
    }

    public BigDecimal getDiscountAmountBczOfRule() {
        return discountAmountBczOfRule;
    }

    public void setDiscountAmountBczOfRule(BigDecimal discountAmountBczOfRule) {
        this.discountAmountBczOfRule = discountAmountBczOfRule;
    }

    public abstract Rule apply(List<ProcessedItem> items);

    protected void setHasOfferOnItems(List<ProcessedItem> items) {
        for (int i = 0; i < items.size(); i++) {
            items.get(i).setOfferApplied(true);
        }
    }

}
