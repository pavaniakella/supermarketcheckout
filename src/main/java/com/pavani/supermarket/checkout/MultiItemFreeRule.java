/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pavani
 */
public class MultiItemFreeRule extends Rule {

    private Item item;
    private Item freeItem;
    private int freeItemQty;

    public MultiItemFreeRule(String ruleName, String ruleCondition, int quantity, Item item, Item freeItem, int freeItemQty) {
        super(ruleName, ruleCondition, quantity);
        checkNotNull(item, "Item cannot be null when creating a multi item free price rule");
        checkNotNull(freeItem, "Free Item cannot be null when creating a multi item free price rule");
        checkArgument(freeItemQty != 0, "free item quantity is required to create a rule");
        this.item = item;
        this.freeItem = freeItem;
        this.freeItemQty = freeItemQty;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Item getFreeItem() {
        return freeItem;
    }

    public void setFreeItem(Item freeItem) {
        this.freeItem = freeItem;
    }

    public int getFreeItemQty() {
        return freeItemQty;
    }

    public void setFreeItemQty(int freeItemQty) {
        this.freeItemQty = freeItemQty;
    }

    @Override
    public Rule apply(List<ProcessedItem> items) {

        List<ProcessedItem> match = new ArrayList<>();
        setDiscountAmountBczOfRule(BigDecimal.ZERO);

        for (ProcessedItem processedItem : items) {
            if (items.contains(processedItem) && !processedItem.isOfferApplied()) {
                match.add(processedItem);
                if (match.size() == quantity) {
                    processItems(items, match);
                    match.clear();
                }
            }
        }
        if (getDiscountAmountBczOfRule() != BigDecimal.ZERO) {
            return this;
        } else {
            return null;
        }
    }

    protected void processItems(List<ProcessedItem> items, List<ProcessedItem> match) {
        List<ProcessedItem> freeMatch = new ArrayList<>();
        for (ProcessedItem processedItem : items) {
            if (!processedItem.isOfferApplied() && processedItem.getSku().equalsIgnoreCase(freeItem.getSku())) {
                freeMatch.add(processedItem);
                if (freeMatch.size() == freeItemQty) {
                    updateFreeItems(freeMatch);
                    setHasOfferOnItems(match);
                    break;
                }
            }
        }
    }

    protected void updateFreeItems(List<ProcessedItem> items) {
        for (ProcessedItem processedItem : items) {
            processedItem.setOfferApplied(true);
            setDiscountAmountBczOfRule(getDiscountAmountBczOfRule().add(freeItem.getPrice()).multiply(new BigDecimal(freeItemQty)));
        }
    }

}
