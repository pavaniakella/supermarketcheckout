/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pavani.supermarket.checkout;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pavani
 */
public class SingleItemRule extends Rule {

    private final Item item;
    private BigDecimal discountPrice;
    private int discountPercentage;

    public SingleItemRule(String ruleName, String ruleCondition, Item item, int quantity, BigDecimal discountPrice, int discountPercentage) {
        super(ruleName, ruleCondition, quantity);
        checkNotNull(item, "Item cannot be null when creating a rule");
        checkArgument((discountPercentage == 0 && discountPrice != null) || (discountPercentage != 0 && discountPrice == null), "discount percentage or discount price only one should be supplied to create a rule");
        this.item = item;
        this.discountPrice = discountPrice;
        this.discountPercentage = discountPercentage;
    }

    public Item getItem() {
        return item;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    @Override
    public Rule apply(List<ProcessedItem> items) {

        List<ProcessedItem> match = new ArrayList<>();
        setDiscountAmountBczOfRule(BigDecimal.ZERO);
        for (ProcessedItem processedItem : items) {
            if (processedItem.getSku().equalsIgnoreCase(item.getSku()) && !processedItem.isOfferApplied()) {
                match.add(processedItem);
                if (match.size() == quantity) {
                    setHasOfferOnItems(match);
                    BigDecimal orginalPrice = processedItem.getPrice().multiply(new BigDecimal(quantity));
                    if (discountPrice != null) {
                        setDiscountAmountBczOfRule(getDiscountAmountBczOfRule().add(orginalPrice.subtract(discountPrice)));
                    } else {
                        setDiscountAmountBczOfRule(getDiscountAmountBczOfRule().add(orginalPrice.multiply(new BigDecimal(discountPercentage).divide(new BigDecimal(100)))));
                    }
                    match.clear();
                }
            }
        }
        if (getDiscountAmountBczOfRule() != BigDecimal.ZERO) {
            return this;
        } else {
            return null;
        }
    }

}
