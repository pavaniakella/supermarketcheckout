Supermarket checkout

DESCRIPTION

Implement a Supermarket checkout that calculates the total price of a number of items.




Some items have multiple prices based on price rules such as:

* buy 3 (equals) items and pay for 2
* buy 2 (equals) items for a special price
* buy 3 (in a set of items) and the cheapest is free
* for each N (equals) items X, you get K items Y for free
* The output required is the receipt with the actual price of every item and the grand total.



CLASSES

Basket

Represents the shopping basket. It contains the list of items and rules. It prints the receipt when calculateTotal method is called.

Payable
interface which is implemented by Item and Rule so that they both can be part of Shopping receipt

Item

Represents an item with an identifier and a price. It has a flag which is true when the item is part of an offer.

ProcessedItem

this is the subclass of Item which has an extra field to track whether offer is applied or not

Rule

Represents an active rule. It participates to compute the price of basket items.


SingleItemRule, MultipleItemRule, MultiItemFreeRule

Rule subclasses which implement different strategies to discount items including discount based on percentages

HOW TO USE

Create a basket, add one or more items, add one or more rules, then invoke checkout to get the receipt.
Example:

		
```
#!java

        Basket basket = new Basket();
		private static final Item APPLE = new Item("SKUAPPLE1", "GALA APPLE", new BigDecimal(0.70));
    	private static final Item ORANGE = new Item("SKUORANGE1", "ORANGE", new BigDecimal(0.50));
   		private static final Item PEAR = new Item("SKUPEAR1", "PEAR", new BigDecimal(0.60));
  		List<Rule> pricingRules = new ArrayList<>();
        Rule buy3Pay2 = new SingleItemRule("buy3for2", "buy3for2", PEAR, 3, new BigDecimal(PEAR.getPrice().doubleValue() * 2), 0);
        pricingRules.add(buy3Pay2);
        Transaction transaction = basket.startTransaction(pricingRules)
                .scan(PEAR)
                .scan(PEAR)
                .scan(ORANGE)
                .scan(PEAR);

        assertEquals(new BigDecimal(1.70).setScale(2, RoundingMode.HALF_UP), basket.calculateTotalPrice(transaction).setScale(2, RoundingMode.HALF_UP));


```



Sample Output for tests is 

```
#!html



====================================================
PEAR ----------------------------  0.60
PEAR ----------------------------  0.60
GALA APPLE ----------------------  0.70
GALA APPLE ----------------------  0.70
PEAR ----------------------------  0.60
PEAR ----------------------------  0.60
ORANGE --------------------------  0.50
PEAR ----------------------------  0.60
PEAR ----------------------------  0.60
GALA APPLE ----------------------  0.70
buy3for2 ------------------------ -1.20
buy2PayX ------------------------ -0.20
====================================================
Total =====4.80
====================================================
```

PRICES & RULES

Prices are represented with BigDecimal to prevent rounding errors.

Multiple rules can be applied to the same basket.

Rules are applied sequentially, so the first rule has the highest priority.

No more than one rule is applied to the same items.

NOTES

Unit tests can be executed with Maven or Eclipse using JUnit.
when an item has more than one pricing rule, the order of the rule application is not consdiered